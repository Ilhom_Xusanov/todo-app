# Todo app

## Description
This is a study purpose project that is written in DRF to serve as an API where users can add their tasks and manage, delete, change their status when they complete them and also filter them according to date and status

## Installation
All the required packages are listed in requirements.txt file. You can create virtual environment and install all packages using
"pip install -r requirements.txt". 

## Usage
You can run the project typing "python manage.py runserver" locally. Also, you can go to /api-schema/docs/   url to test the endpoints through swagger. 

###Note
When sending post or put request give the date in a 'dd/mm/YYYY, HH:MM'  format.  
When filtering by date provide date in a YYYY-mm-dd format
