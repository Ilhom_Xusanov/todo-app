from django.contrib.auth.models import User
from rest_framework import serializers
from .models import Task


class TaskSerializer(serializers.ModelSerializer):
    start_time = serializers.DateTimeField(input_formats=['%d/%m/%Y, %H:%M'])
    end_time = serializers.DateTimeField(input_formats=['%d/%m/%Y, %H:%M'])

    class Meta:
        model = Task
        fields = ['id', 'title', 'description', 'start_time', 'end_time', 'status']

