from django.urls import path, include
from . import views


urlpatterns = [
    path('tasks/', include(
        [
            path('', views.TaskListApiView.as_view()),
            path('<int:pk>/', views.TaskDetailApiView.as_view()),
            path('to-do/', views.TodoTasksApiView.as_view()),
            path('expired/', views.ExpiredTasksApiView.as_view()),
            path('in_progress/', views.InProgressTasksApiView.as_view()),
            path('done/', views.CompletedTasksApiView.as_view())
        ]
    ))
]
