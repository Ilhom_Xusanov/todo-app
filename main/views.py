from datetime import datetime
from django.shortcuts import render
from rest_framework import generics
from django_filters.rest_framework import DjangoFilterBackend
from .models import Task
from.serializers import TaskSerializer


class TaskListApiView(generics.ListCreateAPIView):
    serializer_class = TaskSerializer

    def get_queryset(self):
        queryset = Task.objects.all()
        start_date = self.request.query_params.get('start_date')
        end_date = self.request.query_params.get('end_date')
        if start_date is not None and end_date is not None:
            queryset = queryset.filter(start_time__date=start_date).filter(end_time__date=end_date)
        return queryset


    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class TaskDetailApiView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer


class TodoTasksApiView(generics.ListAPIView):
    queryset = Task.objects.filter(status='todo')
    serializer_class = TaskSerializer


class ExpiredTasksApiView(generics.ListAPIView):
    queryset = Task.objects.filter(status='todo').filter(end_time__lt=datetime.now())
    serializer_class = TaskSerializer


class InProgressTasksApiView(generics.ListAPIView):
    queryset = Task.objects.filter(status='in_progress')
    serializer_class = TaskSerializer


class CompletedTasksApiView(generics.ListAPIView):
    queryset = Task.objects.filter(status='done')
    serializer_class = TaskSerializer